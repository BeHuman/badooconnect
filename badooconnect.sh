#!/bin/bash

CMPT=0
STATUS=1
function scanProfile() {
    date=`date`
    out=`curl -s "https://badoo.com/profile/$1" -A "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0" | grep "\-\-online"`
    if [[ $? -eq 0 ]]; then
        if [[ $STATUS -eq 1 ]]; then
            echo "Connecter sur Badoo le $date"
            echo "Status : online"
        fi
        CMPT=$((CMPT+1))
        STATUS=0
    else 
        out=`curl -s "https://badoo.com/profile/$1" -A "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0" | grep "\-\-away"`
        if [[ $? -eq 0 ]]; then
            if [[ $STATUS -eq 1 ]]; then
                echo "Connecter sur Badoo le $date"
                echo "Status : away"
            fi
            CMPT=$((CMPT+1))
            STATUS=0
        else
            if [[ $STATUS -eq 0 ]]; then
                seconde=$((CMPT*10))
                if [[ $seconde -ge 60 ]]; then
                    min=$((seconde/60))
                    if [[ $min -eq 1 ]]; then
                        m="minute"
                    else
                        m="minutes"
                    fi
                    echo "Temps de connection : $min $m"
                else
                    echo "Temps de connection : $seconde secondes"
                fi
                echo "Deconnection le $date"
                echo ""
                CMPT=0
                STATUS=1
            fi
            
        fi
    fi
    sleep 10
    scanProfile "$1"
}
scanProfile "$1"
