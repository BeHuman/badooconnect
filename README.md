# BadooConnect

Scan un profil Badoo pour récupérer le temps de connexion.

INSTALLATION
============
make install

UTILISATION
============
## Terminal output
```
:~$ badooconnect "Badoo ID"
```
## File output
```
:~$ badooconnect "Badoo ID" > recap.txt
```

UNINSTALLATION
==============
make uninstall
